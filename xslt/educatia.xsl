<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
            </head>
            <body>
                <h1>
                    Educatia
                </h1>
                <br/>
                <h2>
                Total subjects:
                <xsl:value-of select="count(//subject)"/>
                <xsl:apply-templates select="book"/>
                </h2>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="book/subject">
        <div>
            <h3>
                <xsl:value-of select="@id"/>.
            </h3>
            <xsl:for-each select="chapter">
                <xsl:value-of select="theme"/>.
                <xsl:value-of select="author"/>.
                <xsl:value-of select="href"/>
                <br/>
            </xsl:for-each>
            <hr/>
        </div>
    </xsl:template>

</xsl:stylesheet>